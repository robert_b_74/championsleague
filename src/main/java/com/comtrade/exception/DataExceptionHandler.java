package com.comtrade.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class DataExceptionHandler {
	@ExceptionHandler
	public ResponseEntity<DataErrorResponse> handleExceptio404(Exception e){
		DataErrorResponse dataErrorResponse=new DataErrorResponse();
		dataErrorResponse.setId(HttpStatus.NOT_FOUND.value());
		dataErrorResponse.setMessage(e.getMessage());
		dataErrorResponse.setTimeStamp(System.currentTimeMillis());
		
		return new ResponseEntity<DataErrorResponse>(dataErrorResponse, HttpStatus.NOT_FOUND);		
	}

}
