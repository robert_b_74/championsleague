package com.comtrade.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataErrorResponse {
	
	
	private int id;
	private String message;
	private long timeStamp;	

}
