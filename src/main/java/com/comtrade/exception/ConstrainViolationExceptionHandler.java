package com.comtrade.exception;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ConstrainViolationExceptionHandler {
	
	@ExceptionHandler(ConstraintViolationException.class)
	public ResponseEntity<?> handle(ConstraintViolationException constraintViolationException){
		Set<ConstraintViolation<?>> violations=constraintViolationException.getConstraintViolations();
		
		String errorMessage="";
		if(!violations.isEmpty()) {
			StringBuilder builder=new StringBuilder();
			violations.forEach(violation -> builder.append("\n" + violation.getMessage()));
			errorMessage=builder.toString();
		}else {
			errorMessage="ConstraintViolationException occured.";
		}
		return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);		
	}
}
