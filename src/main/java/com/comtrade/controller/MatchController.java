package com.comtrade.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.comtrade.dto.MatchDTO;
import com.comtrade.dto.GroupStandingsDTO;
import com.comtrade.dto.LeagueStandingsDTO;
import com.comtrade.service.MatchService;

@Validated
@RestController
@RequestMapping("/api/v02/")
public class MatchController {
	
	private MatchService matchService;
		
	@Autowired
	public MatchController(MatchService matchService) {
		super();
		this.matchService = matchService;
	}
	@PostMapping("/input/match")
	public ResponseEntity<LeagueStandingsDTO> inputMatch(@Valid @RequestBody List<MatchDTO> matchDTOs){
		LeagueStandingsDTO leageStandingsDTO=matchService.getLeagueSandings(matchDTOs);
		return new ResponseEntity<LeagueStandingsDTO>(leageStandingsDTO, HttpStatus.CREATED);
	}
}
