package com.comtrade.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.comtrade.dto.GroupStandingsDTO;
import com.comtrade.dto.LeagueStandingsDTO;
import com.comtrade.service.StandingService;
@RestController
@RequestMapping("/api/v02/standings/")
public class StandingsController {
		
	private StandingService standingService;
	@Autowired
	public StandingsController(StandingService standingService) {		
		this.standingService=standingService;
	}
	
	@GetMapping("{league}/{season}")
	public ResponseEntity<LeagueStandingsDTO> getLeagueStandings(@PathVariable String league,
			@PathVariable String season){
		LeagueStandingsDTO leagueStandingsDTO=standingService.getLeagueStanding(league, season);		
		return new ResponseEntity<LeagueStandingsDTO>(leagueStandingsDTO, HttpStatus.OK);		
	}
	@GetMapping("{league}/{season}/{group}")
	public ResponseEntity<GroupStandingsDTO> getGroupStandings(
			@PathVariable String league, @PathVariable String season,
			@PathVariable String group){
		GroupStandingsDTO groupStandingsDTO=standingService.getGroupStanding(league, season, group);
		return new ResponseEntity<GroupStandingsDTO>(groupStandingsDTO, HttpStatus.OK);		
	}
}
