package com.comtrade.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.comtrade.dto.FilterDTO;
import com.comtrade.dto.MatchDTO;
import com.comtrade.service.ResoultFilterService;

@Validated
@RestController
@RequestMapping("/api/v02/")
public class ResoultFilterController {
	
	private ResoultFilterService resoultFilerService;
	
	@Autowired
	public ResoultFilterController(ResoultFilterService resoultFilerService) {
		
		this.resoultFilerService = resoultFilerService;
	}
	
	@PostMapping("/filter")
	public ResponseEntity<List<MatchDTO>> filterResoult(@Valid @RequestBody FilterDTO filterDto){
		List<MatchDTO> matchList=resoultFilerService.getMatchList(filterDto);
		System.out.println(filterDto.toString());
		return new ResponseEntity<List<MatchDTO>>(matchList, HttpStatus.OK);		
	}
	

}
