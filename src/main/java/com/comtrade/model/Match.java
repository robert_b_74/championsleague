package com.comtrade.model;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@ToString
public class Match {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;	
	private int matchday;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_group")
	private Group group;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_homeTeam")
	private Team homeTeam;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_awayTeam")
	private Team awayTeam;
	@JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
	private Date kickoffAt;
	private Integer homeTeamScore;
	private Integer awayTeamScore;
	
	
}
