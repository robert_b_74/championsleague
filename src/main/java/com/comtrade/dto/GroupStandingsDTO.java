package com.comtrade.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GroupStandingsDTO {
	
	private String leagueTitle;
	private String season;
	private int matchday;
	private String group;	
	private List<TeamStandingDTO> teamStanding; 

}
