package com.comtrade.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LeagueStandingsDTO {
	private String leagueTitle;
	private String season;
	private List<GroupStandingsDTO> groupStandings;
}
