package com.comtrade.dto;

import java.util.Date;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MatchDTO {
	@NotBlank(message = "League Title must not be blank")	
	private String leagueTitle;
	@NotBlank(message="season must not be blank")
	private String season;
	@Min(value = 1)
	@Max(value = 10)
	private int matchday;
	@NotBlank(message = "Group must not be blank")
	private String group;
	@NotBlank(message = "Home Team must not be blank")
	private String homeTeam;
	@NotBlank(message = "Away Team must not be blank")
	private String awayTeam;
	@JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
	private Date kickoffAt;
	@NotBlank(message = "Final score must not be blank")
	private String score;
	
}
