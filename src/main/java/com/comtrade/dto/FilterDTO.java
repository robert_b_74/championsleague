package com.comtrade.dto;

import java.util.Date;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class FilterDTO {
	
	@NotBlank(message="League Title must not be blank")
	private String leagueTitle;
	@NotBlank(message="Season must not be blank")
	private String season;
	private String team;
	private String group;
	@JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
	private Date startDate;
	@JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
	private Date endDate;

}
