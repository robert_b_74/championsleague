package com.comtrade.mapper.impl;

import org.springframework.stereotype.Component;

import com.comtrade.dto.MatchDTO;
import com.comtrade.mapper.MatchMapper;
import com.comtrade.model.Group;
import com.comtrade.model.League;
import com.comtrade.model.Match;
import com.comtrade.model.Team;

@Component
public class MatchMapperImpl implements MatchMapper {
	
	@Override
	public MatchDTO toMatchDTO(Match match) {
		MatchDTO matchDTO=new MatchDTO();
		matchDTO.setLeagueTitle(match.getGroup().getLeague().getLeagueTitle());
		matchDTO.setSeason(match.getGroup().getLeague().getSeason());
		matchDTO.setMatchday(match.getMatchday());
		matchDTO.setGroup(match.getGroup().getGroupName());
		matchDTO.setHomeTeam(match.getHomeTeam().getTeamName());
		matchDTO.setAwayTeam(match.getAwayTeam().getTeamName());
		matchDTO.setKickoffAt(match.getKickoffAt());
		matchDTO.setScore(match.getHomeTeamScore().toString()+":"+match.getAwayTeamScore().toString());
		return matchDTO;
	}

	@Override
	public Match toMatch(MatchDTO matchDTO) {
		League league=new League(matchDTO.getLeagueTitle(),matchDTO.getSeason());
		Group group=new Group(matchDTO.getGroup(), league);
		Team homeTeam=new Team(matchDTO.getHomeTeam());
		Team awayTeam=new Team(matchDTO.getAwayTeam());
		Match match=new Match();
		
		match.setMatchday(matchDTO.getMatchday());				
		match.setGroup(group);		
		match.setHomeTeam(homeTeam);		
		match.setAwayTeam(awayTeam);
		match.setKickoffAt(matchDTO.getKickoffAt());
		String[]score=matchDTO.getScore().split(":");
		match.setHomeTeamScore(Integer.parseInt(score[0]));
		match.setAwayTeamScore(Integer.parseInt(score[1]));
		return match;
	}
}
