package com.comtrade.mapper;

import org.mapstruct.Mapper;

import com.comtrade.dto.MatchDTO;
import com.comtrade.model.Match;

@Mapper
public interface MatchMapper {
	
	MatchDTO toMatchDTO(Match match);
	Match toMatch(MatchDTO match);
	
}
