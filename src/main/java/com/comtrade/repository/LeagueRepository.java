package com.comtrade.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.comtrade.model.League;

@Repository
public interface LeagueRepository extends CrudRepository<League, Integer> {

	League findByLeagueTitle(String league);

	League findByLeagueTitleAndSeason(String leagueTitle, String season);

}
