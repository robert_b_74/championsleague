package com.comtrade.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.comtrade.model.Group;

@Repository
public interface GroupRepository extends JpaRepository<Group, Integer> {
	
	@Query("select g from Group g where g.groupName=:group and g.league.id=:id")
	Group findGroupByNameAndLeagueId(@Param("group") String group,@Param("id") Integer id);
	
	@Query("SELECT g FROM Group g WHERE g.groupName =:group")
	Group findByGroupName(@Param("group")String group);

	Group findByGroupNameAndLeagueId(String group, Integer id);
	
	@Query("select g from Group g where g.groupName = :group and g.league.leagueTitle = :league and g.league.season = :season")
	Group findByGroupNameAndLeagueTitleAndSeason(@Param("group")String group, @Param("league")String league,@Param("season")String season);
	
}
