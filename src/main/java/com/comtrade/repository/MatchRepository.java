package com.comtrade.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.comtrade.dto.FilterDTO;
import com.comtrade.model.Group;
import com.comtrade.model.Match;

@Repository
public interface MatchRepository extends JpaRepository<Match, Integer> {
	
	List<Match> findByGroup(Group groupRepo);
	
	Match findFirstByGroupIdAndMatchdayAndHomeTeamIdAndAwayTeamId(Integer id, int matchday, Integer id2, Integer id3);
	
	@Query("SELECT m FROM Match m where m.group.league.id = :id")
	List<Match> findByLeagueId(@Param("id")Integer leagueId);
	
	@Query("SELECT m FROM Match m where m.group.league.id = :id and kickoff_at between :sDate and :eDate")
	List<Match> findByLeagueIdAndDateSpan(@Param("id")Integer leagueId,@Param("sDate")Date startDate,@Param("eDate") Date endDate);
	
	@Query("SELECT m FROM Match m where m.group.id = :id and kickoff_at between :sDate and :eDate")
	List<Match> findByGroupIdAndDateSpan(@Param("id")Integer groupId, @Param("sDate")Date startDate,@Param("eDate") Date endDate);

	@Query("SELECT m FROM Match m where (m.homeTeam.id = :id or m.awayTeam.id = :id) and kickoff_at between :sDate and :eDate")
	List<Match> findByTeamIdAndDateSpan(@Param("id")Integer teamId, @Param("sDate")Date startDate,@Param("eDate") Date endDate);

	@Query("SELECT m FROM Match m where m.group.id = :idg and (m.homeTeam.id = :idt or m.awayTeam.id = :idt) and kickoff_at between :sDate and :eDate")
	List<Match> findByGroupIdAndTeamIdAndDateSpan(@Param("idg")Integer groupId, @Param("idt")Integer teamId, @Param("sDate")Date startDate, @Param("eDate") Date endDate);

	

}
