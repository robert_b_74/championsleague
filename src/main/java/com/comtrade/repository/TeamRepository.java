package com.comtrade.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.comtrade.model.Team;

@Repository
public interface TeamRepository extends CrudRepository<Team, Integer> {

	Team findByTeamName(String homeTeam);

}
