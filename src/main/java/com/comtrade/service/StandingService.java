package com.comtrade.service;

import java.util.List;

import com.comtrade.dto.GroupStandingsDTO;
import com.comtrade.dto.LeagueStandingsDTO;
import com.comtrade.model.Match;

public interface StandingService {

	LeagueStandingsDTO getLeagueStanding(String league, String season);
	
	GroupStandingsDTO getGroupStanding(String league, String season, String group);
	
	GroupStandingsDTO groupStandingBuild(List<Match> match);
	LeagueStandingsDTO leagueStandingBuild(List<Match> match);

}
