package com.comtrade.service;

import java.util.List;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.comtrade.dto.MatchDTO;
import com.comtrade.dto.LeagueStandingsDTO;
import com.comtrade.mapper.MatchMapper;
import com.comtrade.model.Group;
import com.comtrade.model.League;
import com.comtrade.model.Match;
import com.comtrade.model.Team;
import com.comtrade.repository.GroupRepository;
import com.comtrade.repository.LeagueRepository;
import com.comtrade.repository.MatchRepository;
import com.comtrade.repository.TeamRepository;

@Service
public class MatchServiceImpl implements MatchService {
	
	private GroupRepository groupRepository;
	private LeagueRepository leagueRepository;
	private MatchRepository matchRepository;
	private TeamRepository teamRepository;
	private MatchMapper matchMapper;
	private StandingService standingService;

	
	
	@Autowired
	public MatchServiceImpl(GroupRepository groupRepository, LeagueRepository leagueRepository,
			MatchRepository matchRepository, TeamRepository teamRepository,
			MatchMapper matchMapper, StandingService standingService) {
		super();
		this.groupRepository = groupRepository;
		this.leagueRepository = leagueRepository;
		this.matchRepository = matchRepository;
		this.teamRepository = teamRepository;
		this.matchMapper=matchMapper;
		this.standingService=standingService;
	}

	@Override
	@Transactional
	public LeagueStandingsDTO getLeagueSandings(List<MatchDTO> matchDTOs) {
		Integer leagueId=0;
		for (MatchDTO matchDTO : matchDTOs) {
			Match match=matchMapper.toMatch(matchDTO);
			League leagueRepo=leagueRepository.findByLeagueTitleAndSeason(matchDTO.getLeagueTitle(),matchDTO.getSeason());
			if(leagueRepo==null) {
				leagueRepo=leagueRepository.save(match.getGroup().getLeague());
			}
			leagueId=leagueRepo.getId();
			match.getGroup().setLeague(leagueRepo);
			Group groupRepo=groupRepository.findByGroupNameAndLeagueId(matchDTO.getGroup(), leagueRepo.getId());
			if(groupRepo==null) {
				groupRepo=groupRepository.save(match.getGroup());
			}
			match.setGroup(groupRepo);
			Team teamHomeRepo=teamRepository.findByTeamName(matchDTO.getHomeTeam());
			if(teamHomeRepo==null) {
				teamHomeRepo=teamRepository.save(match.getHomeTeam());
			}
			match.setHomeTeam(teamHomeRepo);
			
			Team teamAwayRepo=teamRepository.findByTeamName(matchDTO.getAwayTeam());
			if(teamAwayRepo==null) {
				teamAwayRepo=teamRepository.save(match.getAwayTeam());
			}
			match.setAwayTeam(teamAwayRepo);			
			
			//Check if current match is all ready in DB, if not, persist!
			Match matchRepo=matchRepository.findFirstByGroupIdAndMatchdayAndHomeTeamIdAndAwayTeamId(match.getGroup().getId(), match.getMatchday(), match.getHomeTeam().getId(), match.getAwayTeam().getId());
			if(matchRepo==null) {
				matchRepository.save(match);
			}
		}
		List<Match> allMatch=matchRepository.findByLeagueId(leagueId);
		
		return standingService.leagueStandingBuild(allMatch);
	}

}
