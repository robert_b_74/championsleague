package com.comtrade.service;

import java.util.List;

import com.comtrade.dto.FilterDTO;
import com.comtrade.dto.MatchDTO;

public interface ResoultFilterService {

	List<MatchDTO> getMatchList(FilterDTO filterDto);

}
