package com.comtrade.service;

import java.util.List;

import com.comtrade.dto.MatchDTO;
import com.comtrade.dto.GroupStandingsDTO;
import com.comtrade.dto.LeagueStandingsDTO;

public interface MatchService {

	LeagueStandingsDTO getLeagueSandings(List<MatchDTO> match);

}
