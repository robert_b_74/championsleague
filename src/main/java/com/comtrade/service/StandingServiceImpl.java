package com.comtrade.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.comtrade.exception.DataNotFoundException;
import com.comtrade.dto.GroupStandingsDTO;
import com.comtrade.dto.LeagueStandingsDTO;
import com.comtrade.dto.TeamStandingDTO;
import com.comtrade.model.Group;
import com.comtrade.model.League;
import com.comtrade.model.Match;
import com.comtrade.repository.GroupRepository;
import com.comtrade.repository.LeagueRepository;
import com.comtrade.repository.MatchRepository;

@Service
public class StandingServiceImpl implements StandingService{
	
	private GroupRepository groupRepository;
	private MatchRepository matchRepository;	
	private LeagueRepository leagueRepository;	
	
	@Autowired
	public StandingServiceImpl(GroupRepository groupRepository, MatchRepository matchRepository, 
			LeagueRepository leagueRepository) {
		super();
		this.groupRepository = groupRepository;
		this.matchRepository = matchRepository;		
		this.leagueRepository=leagueRepository;
	}

	@Override
	public LeagueStandingsDTO getLeagueStanding(String league, String season) {
		League leagueRepo=leagueRepository.findByLeagueTitleAndSeason(league, season);
		if(leagueRepo==null) {
			throw new DataNotFoundException("No Data found for given paramaters league: "
					+league+" and season: "+season);
		}
		List<Match>match=matchRepository.findByLeagueId(leagueRepo.getId());
		return leagueStandingBuild(match);
	}

	@Override
	public GroupStandingsDTO getGroupStanding(String league, String season, String group) {
		Group groupRepo=groupRepository.findByGroupNameAndLeagueTitleAndSeason(group,league,season);
		if(groupRepo==null) {
			throw new DataNotFoundException("No Data found for given parameters"
					+ " league: "+league+", season: "+season+
					", group: "+group);
		}
		List<Match>match=matchRepository.findByGroup(groupRepo);
		return groupStandingBuild(match);
	}

	@Override
	public GroupStandingsDTO groupStandingBuild(List<Match> groupMatch) {

		GroupStandingsDTO groupStanding=new GroupStandingsDTO();			
		HashMap<String, TeamStandingDTO> mapa=new HashMap<String, TeamStandingDTO>();
		List<TeamStandingDTO> teamStandingList = new ArrayList<>();		
		Integer matchday=0;
		
		for (Match match : groupMatch) {
			mapa.put(match.getHomeTeam().getTeamName(), new TeamStandingDTO(0, null, 0, 0, 0, 0, 0, 0, 0, 0));
			mapa.put(match.getAwayTeam().getTeamName(), new TeamStandingDTO(0, null, 0, 0, 0, 0, 0, 0, 0, 0));
			groupStanding.setLeagueTitle(match.getGroup().getLeague().getLeagueTitle());
			groupStanding.setGroup(match.getGroup().getGroupName());
			groupStanding.setSeason(match.getGroup().getLeague().getSeason());
			if(match.getMatchday()>matchday) matchday=match.getMatchday();
		}
		
		for (Match match : groupMatch) {		
			
			TeamStandingDTO sHome = mapa.get(match.getHomeTeam().getTeamName()); 
			TeamStandingDTO sAway = mapa.get(match.getAwayTeam().getTeamName());
			 
			
			sHome.setTeam(match.getHomeTeam().getTeamName());
			sHome.setPlayedGames(sHome.getPlayedGames()+1);
			sAway.setTeam(match.getAwayTeam().getTeamName());
			sAway.setPlayedGames(sAway.getPlayedGames()+1);
			if(match.getHomeTeamScore()>match.getAwayTeamScore()) {
				sHome.setPoints(sHome.getPoints()+3);				
				sHome.setWin(sHome.getWin()+1);
				sAway.setLose(sAway.getLose()+1);
			}else if(match.getHomeTeamScore()==match.getAwayTeamScore()) {
				sHome.setPoints(sHome.getPoints()+1);
				sAway.setPoints(sAway.getPoints()+1);
				sHome.setDraw(sHome.getDraw()+1);
				sAway.setDraw(sAway.getDraw()+1);
			}else {
				sAway.setPoints(sAway.getPoints()+3);				
				sAway.setWin(sAway.getWin()+1);
				sHome.setLose(sHome.getLose()+1);
			}
			sHome.setGoals(sHome.getGoals()+match.getHomeTeamScore());
			sHome.setGoalsAgainst(sHome.getGoalsAgainst()+match.getAwayTeamScore());
			sHome.setGoalDifference(sHome.getGoals()-sHome.getGoalsAgainst());
			sAway.setGoals(sAway.getGoals()+match.getAwayTeamScore());
			sAway.setGoalsAgainst(sAway.getGoalsAgainst()+match.getHomeTeamScore());
			sAway.setGoalDifference(sAway.getGoals()-sAway.getGoalsAgainst());
			mapa.put(match.getHomeTeam().getTeamName(), sHome);
			mapa.put(match.getAwayTeam().getTeamName(), sAway);
		}		
		
		for(TeamStandingDTO teamStandingDTO: mapa.values()) {
			teamStandingList.add(teamStandingDTO);
		}		
		
		Collections.sort(teamStandingList, comparator);
		Integer i=1;
		for (TeamStandingDTO teamStandingDTO : teamStandingList) {			
			teamStandingDTO.setRank(i);
			i++;
		}
		
		groupStanding.setMatchday(matchday);
		groupStanding.setTeamStanding(teamStandingList);
		return groupStanding;
	}

	@Override
	public LeagueStandingsDTO leagueStandingBuild(List<Match> allMatch) {
		LeagueStandingsDTO leagueStandings=new LeagueStandingsDTO();
		List<GroupStandingsDTO> groupStandings=new ArrayList<>();
		Set<String> groups=new HashSet<>();
		for (Match match : allMatch) {
			leagueStandings.setSeason(match.getGroup().getLeague().getSeason());
			leagueStandings.setLeagueTitle(match.getGroup().getLeague().getLeagueTitle());
			groups.add(match.getGroup().getGroupName());
		}
		
		for (String group : groups) {
			List<Match> grupMatch=new ArrayList<>();
			for (Match match : allMatch) {
				if(match.getGroup().getGroupName().equals(group)) {
					grupMatch.add(match);
				}				
			}
			groupStandings.add(groupStandingBuild(grupMatch));
		}
		leagueStandings.setGroupStandings(groupStandings);
		return leagueStandings;
	}
	Comparator<TeamStandingDTO> comparator=new Comparator<TeamStandingDTO>() {
		@Override
		public int compare(TeamStandingDTO o1, TeamStandingDTO o2) {
			if(!o1.getPoints().equals(o2.getPoints())) {
				return o2.getPoints().compareTo(o1.getPoints());
			}else if(!o1.getGoals().equals(o2.getGoals())) {
				return o2.getGoals().compareTo(o1.getGoals());
			}else {
				return o2.getGoalDifference().compareTo(o1.getGoalDifference());
			}				
		}			
	};
}
