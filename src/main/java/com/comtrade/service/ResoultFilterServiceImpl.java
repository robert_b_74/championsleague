package com.comtrade.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.comtrade.dto.FilterDTO;
import com.comtrade.dto.MatchDTO;
import com.comtrade.exception.DataNotFoundException;
import com.comtrade.mapper.MatchMapper;
import com.comtrade.model.Match;
import com.comtrade.repository.GroupRepository;
import com.comtrade.repository.LeagueRepository;
import com.comtrade.repository.MatchRepository;
import com.comtrade.repository.TeamRepository;

@Service
public class ResoultFilterServiceImpl implements ResoultFilterService {
	
	
	private MatchRepository matchRepository;	
	private MatchMapper matchMapper;
	private LeagueRepository leagueRepository;
	private GroupRepository groupRepository;
	private TeamRepository teamRepository;

	@Autowired
	public ResoultFilterServiceImpl(MatchRepository matchRepository, MatchMapper matchMapper,
			LeagueRepository leagueRepository, GroupRepository groupRepository,
			TeamRepository teamRepository) {
		super();
		this.matchRepository = matchRepository;
		this.matchMapper = matchMapper;
		this.leagueRepository=leagueRepository;
		this.groupRepository=groupRepository;
		this.teamRepository=teamRepository;
	}


	@Override
	public List<MatchDTO> getMatchList(FilterDTO filterDto) {
		List<Match>matchList=new ArrayList<>();
		List<MatchDTO>matchListDto=new ArrayList<>();
		Integer groupId=0;
		Integer teamId=0;
		int querySwitch=111;
		Integer leagueId=leagueRepository.findByLeagueTitleAndSeason(filterDto.getLeagueTitle(), filterDto.getSeason()).getId();
		String group=filterDto.getGroup();
		if(group.isEmpty()) {
			querySwitch-=10;
		}else {
			groupId=groupRepository.findByGroupNameAndLeagueId(group, leagueId).getId();
		}
		String team=filterDto.getTeam();
		if(team.isEmpty()) {
			querySwitch-=1;
		}else {
			teamId=teamRepository.findByTeamName(team).getId();
		}
		
		Date startDate=new Date(1);
		if(!(filterDto.getStartDate()==null)) {
			startDate=filterDto.getStartDate();
		}
		Date endDate=new Date(System.currentTimeMillis());
		if(!(filterDto.getEndDate()==null)) {
			endDate=filterDto.getEndDate();
		}
		switch (querySwitch) {
		case 100:
			matchList=matchRepository.findByLeagueIdAndDateSpan(leagueId, startDate, endDate);
			break;
		case 110:
			matchList=matchRepository.findByGroupIdAndDateSpan(groupId, startDate, endDate);
			break;
			
		case 101:
			matchList=matchRepository.findByTeamIdAndDateSpan(teamId, startDate, endDate);
			break;
		case 111:
			matchList=matchRepository.findByGroupIdAndTeamIdAndDateSpan(groupId, teamId, startDate, endDate);
			break;
		}
		if(matchList.isEmpty()) {
			throw new DataNotFoundException("No Data found for given set of filters");
		}
		for (Match match : matchList) {
			MatchDTO matchDto=matchMapper.toMatchDTO(match);
			matchListDto.add(matchDto);
		}		
		return matchListDto;
	}

}
